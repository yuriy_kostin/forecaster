# forecaster

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

1. docs for vue-loader (https://vue-loader.vuejs.org/).
2. docs for vue-router (https://router.vuejs.org/).


#API: https://www.wunderground.com/weather/api/d/docs?d=data/forecast10day



Vue 2.0 Webpack & vue-loader:
https://github.com/vuejs-templates/webpack-simple

Libs:
https://vuejs.org 2.0
https://d3js.org/
http://yuche.github.io/vue-strap/

Services:
https://vuecomponents.com/
https://vue-loader.vuejs.org

Features:
http://pespantelis.github.io/vue-typeahead -- autocomplite
